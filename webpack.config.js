const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const webpack = require('webpack');

const commonConfig = {
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname),
        publicPath: '/'
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public', 'index.html')
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 8192
                    }
                }]
            }
        ]
    }
};

let devConfig = {
    output: commonConfig.output,
    entry: [
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
        './src/index.js'
    ],
    devtool: 'inline-source-map',
    // devServer: {
    //     contentBase: './dist',
    //     publicPath: '/',
    //     hot: true,
    //     historyApiFallback: true,
    // },
    plugins: commonConfig.plugins.concat([
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]),
    module: {
        rules: commonConfig.module.rules.concat({
            test: /\.css$/,
            use: [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                'postcss-loader'
            ]
        })
    }
};

let prodConfig = {
    output: commonConfig.output,
    entry: './src/index.js',
    plugins: commonConfig.plugins.concat([
        new ExtractTextPlugin({
            filename: 'style.css'
        }),
        new MinifyPlugin()
    ]),
    module: {
        rules: commonConfig.module.rules.concat({
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    'postcss-loader'
                ]
            })
        })
    }
};

module.exports = process.env.NODE_ENV === 'production' ? prodConfig : devConfig

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Header from '../components/Header';
import Drawer from '../components/Drawer';

storiesOf('App', module)
    .add('Header', () => (
        <Header cartQuantity={100} onClickIcon={action('Clicou no header')} />
    ))
    .add('Drawer opened', () => (
        <Drawer open={true}>
            <div style={{
                'height': '100%',
                'width': '100%',
                'background-color': '#222',
                'color': 'white'
            }}
            >
                Abriu drawer
            </div>
        </Drawer>
    ))
    .add('Drawer closed', () => (
        <Drawer open={false}>
            Abriu drawer
        </Drawer>
    ))
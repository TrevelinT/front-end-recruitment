import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import Drawer from './Drawer';
import Cart from '../../Cart/components/Cart';
import { getCartQuantity } from '../../Cart/selectors';
import ProductList from '../../ProductList/components/ProductList';
import '../../Shared/styles/app.css'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            drawerOpen: false,
        }

        this.handleToggleDrawer = this.handleToggleDrawer.bind(this);
    }

    handleToggleDrawer() {
        this.setState(prevState => ({
            drawerOpen: !prevState.drawerOpen
        }));
    }

    render() {
        return (
            <div>
                <Header
                    cartQuantity={this.props.cartQuantity}
                    onClickIcon={this.handleToggleDrawer}
                />
                <div className="c-content">
                    <Drawer open={this.state.drawerOpen}>
                        <div className="c-drawer-content">
                            <Cart />
                        </div>
                    </Drawer>
                    <ProductList />
                </div>
            </div>
        )
    }
}

export { App };

function mapStateToProps(state) {
    return {
        cartQuantity: getCartQuantity(state)
    }
}

export default connect(mapStateToProps)(App);
import React from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/helpers.css';
import '../../Shared/styles/header.css';
import bag from '../../Shared/images/bag.png';
import logo from '../../Shared/images/netshoes-logo.png';

const propTypes = {
    onClickIcon: PropTypes.func.isRequired,
    cartQuantity: PropTypes.number.isRequired
};

function Header({ onClickIcon, cartQuantity }) {
    function handleClick(event) {
        event.preventDefault();
        onClickIcon();
    }

    return (
        <header className="c-header">
            <div className="l-container c-header__content">
                <h1 className="c-header__logo"><img className="c-header__logo-image" src={logo} alt="Netshoes"/></h1>
                <a className="c-header__cart" href="#" onClick={handleClick}>
                    <img src={bag} />
                    <span className="c-cart-header__quantity">{cartQuantity}</span>
                </a>
            </div>
        </header>
    );
}

Header.propTypes = propTypes;

export default Header;
import React from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/drawer.css';

const propTypes = {
    open: PropTypes.bool
};

function Drawer({ open, children }) {
    function getClassName(open) {
        if (open) {
            return ['c-drawer', 'c-drawer--is-open'].join(' ');
        }

        return 'c-drawer';
    }
    
    return (
        <aside className={getClassName(open)}>
            {children}
        </aside>
    )
}

Drawer.propTypes = propTypes;

export default Drawer;
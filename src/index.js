import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/components/App';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import allReducers from './Shared/allReducers';
import { AppContainer } from 'react-hot-loader';
import { loadState, saveState } from './Shared/localStorage';
import throttle from 'lodash.throttle';

const preloadedState = loadState();

const store = createStore(
    allReducers,
    preloadedState,
    compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

store.subscribe(throttle(() => {
    saveState(store.getState());
}, 1000));

const render = Component => {
    ReactDOM.render(
        <AppContainer>
        <Provider store={store}>
                <Component />
        </Provider>
        </AppContainer>,
        document.getElementById('root')
    );
}

render(App);

if (module.hot) {
    module.hot.accept('./App/components/App', () => {
        const NextApp = require('./App/components/App').default;
        render(NextApp)
    });
}

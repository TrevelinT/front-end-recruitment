import { NAME } from './constants';

export function getAllProducts(state) {
    return state[NAME].products;
}

export function getProductById(id, state) {
    return getAllProducts(state)[id];
}
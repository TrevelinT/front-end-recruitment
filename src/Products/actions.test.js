import * as types from './actionTypes'
import * as actions from './actions'

describe('products actions', () => {
    it('should create an action to fetch all products', () => {
        const expectedAction = {
            type: types.FETCH
        };

        expect(actions.fetchAll()).toEqual(expectedAction);
    });

    it('should create an action to fetch products successfully', () => {
        const response = {
            products: [
                { id: 1 },
                { id: 2 }
            ]
        };
        const expectedAction = {
            type: types.FETCH_SUCCESS,
            payload: {
                1: {
                    id: 1,
                    image: 'item01.jpg'
                },
                2: {
                    id: 2,
                    image: 'item02.jpg'
                }
            }
        };

        expect(actions.fetchSuccess(response)).toEqual(expectedAction)
    });

    it('should create an action to catch error when fetch products request', () => {
        const error = new Error();
        const expectedAction = {
            type: types.FETCH_FAILURE,
            payload: error,
            error: true
        };

        expect(actions.fetchFailure(error)).toEqual(expectedAction)
    });
});
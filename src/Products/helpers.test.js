import { normalize, addImageToResponse } from './helpers';
import response from '../../public/data/products.json';

describe('helpers', () => {
    it('Should normalize server response', () => {
        expect(normalize(response.products.slice(0, 2))).toEqual({
            0: {
                "id": 0,
                "sku": 8552515751438644,
                "title": "Camisa Nike Corinthians I",
                "description": "14/15 s/nº",
                "availableSizes": ["S", "G", "GG", "GGG"],
                "style": "Branco com listras pretas",
                "price": 229.9,
                "installments": 9,
                "currencyId": "BRL",
                "currencyFormat": "R$",
                "isFreeShipping": true
            },
            1: {
                "id": 1,
                "sku": 18644119330491312,
                "title": "Camisa Nike Corinthians II",
                "description": "14/15 s/nº",
                "availableSizes": ["S", "G", "GG", "GGG"],
                "style": "Preta com listras brancas",
                "price": 229.9,
                "installments": 9,
                "currencyId": "BRL",
                "currencyFormat": "R$",
                "isFreeShipping": true
            }
        });
    });

    it('Should add image to server response', () => {
        expect(addImageToResponse(['item01.jpg', 'item02.jpg'],response.products.slice(0, 2))).toEqual([
            {
                "id": 0,
                "sku": 8552515751438644,
                "title": "Camisa Nike Corinthians I",
                "description": "14/15 s/nº",
                "availableSizes": ["S", "G", "GG", "GGG"],
                "style": "Branco com listras pretas",
                "price": 229.9,
                "installments": 9,
                "currencyId": "BRL",
                "currencyFormat": "R$",
                "isFreeShipping": true,
                "image": 'item01.jpg',
            },
            {
                "id": 1,
                "sku": 18644119330491312,
                "title": "Camisa Nike Corinthians II",
                "description": "14/15 s/nº",
                "availableSizes": ["S", "G", "GG", "GGG"],
                "style": "Preta com listras brancas",
                "price": 229.9,
                "installments": 9,
                "currencyId": "BRL",
                "currencyFormat": "R$",
                "isFreeShipping": true,
                "image": 'item02.jpg',
            }
        ]);
    });
});
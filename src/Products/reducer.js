import merge from 'lodash.merge';
import * as types from './actionTypes';

const initialState = {
    isFetching: false,
    error: null,
    products: {},
}

export default function productsReducer(previousState, { type, payload }) {
    previousState = previousState || initialState;

    switch (type) {
        case types.FETCH:
            return {
                ...previousState,
                isFetching: true,
                error: null
            };
        case types.FETCH_FAILURE:
            return {
                ...previousState,
                isFetching: false,
                error: payload
            }

        case types.FETCH_SUCCESS:
            return {
                ...previousState,
                isFetching: false,
                error: null,
                products: merge({}, previousState.products, payload)
            }
    
        default:
            return previousState;
    }
}
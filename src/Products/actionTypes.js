import { NAME } from './constants';

export const FETCH_SUCCESS = `${NAME}/FETCH_SUCCESS`;
export const FETCH_FAILURE = `${NAME}/FETCH_FAILURE`;
export const FETCH = `${NAME}/FETCH`;
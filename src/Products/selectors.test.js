import configureStore from 'redux-mock-store'
import { NAME } from './constants'
import { getAllProducts, getProductById } from './selectors';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('products selectors', () => {
    it('should get all products', () => {
        const products = {
            1: { id: 1 },
            2: { id: 2 }
        }
        const store = mockStore({
            [NAME]: {
                products 
            }
        });

        expect(getAllProducts(store.getState())).toEqual(products);
    })

    it('should get a product by id', () => {
        const products = {
            1: { id: 1 },
            2: { id: 2 }
        }
        const store = mockStore({
            [NAME]: {
                products
            }
        });

        expect(getProductById(2, store.getState())).toEqual(products[2]);
    });
});
import * as types from './actionTypes';
import { normalize, addImageToResponse } from './helpers';
import { getAllProducts } from './selectors';
import data from '../../public/data/products.json';

const IMAGES = [
    'item01.jpg',
    'item02.jpg',
    'item03.jpg',
    'item04.jpg',
    'item05.jpg',
    'item06.jpg',
    'item07.jpg',
    'item08.jpg',
    'item09.jpg',
];

export function fetchAll() {
    return {
        type: types.FETCH
    };
}

export function fetchSuccess(response) {
    return {
        type: types.FETCH_SUCCESS,
        payload: normalize(addImageToResponse([].concat(IMAGES, IMAGES), response.products))
    };
}

export function fetchFailure(error) {
    return {
        type: types.FETCH_FAILURE,
        payload: error,
        error: true
    };
}

export function fetchProductListIfNeeded() {
    return function(dispatch, getState) {
        if (!Object.keys(getAllProducts(getState())).length) {
            dispatch(fetchAll())
            fetch('/api/products')
                .then(response =>  response.json())
                .then(data => {
                    dispatch(fetchSuccess(data))
                })
                .catch(() => {
                    dispatch(fetchFailure())
                })
        }
    }
}
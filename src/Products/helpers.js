export function normalize(response) {
    return response.reduce(
        (products, product) => ({
            ...products,
            [product.id]: product
        }),
    {})
}

export function addImageToResponse(images, response) {
    return response.map((product, index) => ({
        ...product,
        image: images[index]
    }))
}
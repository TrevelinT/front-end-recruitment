import * as actions from './actions';
import reducer from './reducer';

const initialState = {
    isFetching: false,
    error: null,
    products: {}
}

describe('products reducer', () => {
    it('Should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('Should handle FETCH', () => {
        expect(reducer(initialState, actions.fetchAll())).toEqual({
            ...initialState,
            isFetching: true,
        });
    });

    it('Should handle FETCH_SUCCESS', () => {
        const response = {
            products: [
                { id: 1 },
                { id: 2 }
            ]
        }
        const products = {
            1: { id: 1, image: 'item01.jpg' },
            2: { id: 2, image: 'item02.jpg' }
        };

        expect(reducer(initialState, actions.fetchSuccess(response))).toEqual({
            ...initialState,
            products
        });
    });

    it('Should handle FETCH_FAILURE', () => {
        const error = new Error();

        expect(reducer(initialState, actions.fetchFailure(error))).toEqual({
            ...initialState,
            error,
        });
    });
})
import * as types from './actionTypes'
import * as actions from './actions'

describe('cart actions', () => {
    it('should create an action to add an item to cart', () => {
        const payload = {
            item: '9128382792839',
            quantity: 2
        };
        const expectedAction = {
            type: types.ADD_ITEM,
            payload
        };

        expect(actions.addItem(payload.item, payload.quantity)).toEqual(expectedAction)
    });

    it('should create an action to remove an item of the cart', () => {
        const removedItem = '9128382792839';
        const expectedAction = {
            type: types.REMOVE_ITEM,
            payload: {
                item: removedItem
            }
        };

        expect(actions.removeItem(removedItem)).toEqual(expectedAction)
    });

    it('should create an action to change an item quantity of the cart', () => {
        const payload = {
            item: '9128382792839',
            quantity: 4
        };
        const expectedAction = {
            type: types.CHANGE_ITEM_QUANTITY,
            payload
        };

        expect(actions.changeItemQuantity(payload.item, payload.quantity)).toEqual(expectedAction)
    });
});
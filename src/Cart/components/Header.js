import React from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/cart.css';
import bag from '../../Shared/images/bag.png';

const propTypes = {
    productQuantity: PropTypes.number
}

function Header({ productQuantity }) {
    return (
        <header className="c-cart-header">
            <h1 className="c-cart-header__title">
                <span className="c-cart-header__icon">
                    <img src={bag} />
                    <span className="c-cart-header__quantity">{productQuantity}</span>
                </span> Sacola
            </h1>
        </header>
    )
}

Header.propTypes = propTypes;

export default Header;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/cart.css';
import '../../Shared/styles/mediaObject.css';
import '../../Shared/styles/price.css';
import item01 from '../../Shared/images/item01.jpg'
import item02 from '../../Shared/images/item02.jpg'
import item03 from '../../Shared/images/item03.jpg'
import item04 from '../../Shared/images/item04.jpg'
import item05 from '../../Shared/images/item05.jpg'
import item06 from '../../Shared/images/item06.jpg'
import item07 from '../../Shared/images/item07.jpg'
import item08 from '../../Shared/images/item08.jpg'
import item09 from '../../Shared/images/item09.jpg'

const images = {
    'item01.jpg': item01,
    'item02.jpg': item02,
    'item03.jpg': item03,
    'item04.jpg': item04,
    'item05.jpg': item05,
    'item06.jpg': item06,
    'item07.jpg': item07,
    'item08.jpg': item08,
    'item09.jpg': item09,
}

const propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    onRemove: PropTypes.func.isRequired
}

class CartItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hover: false,
        }

        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
    }

    handleMouseEnter() {
        if (this.state.hover === false) {
            this.setState({
                hover: true
            });
        }
    }

    handleMouseLeave() {
        if (this.state.hover === true) {
            this.setState({
                hover: false
            });
        }
    }

    getPrice(price) {
        return price.toFixed(2).split('.')
    }

    renderPrice(price, currencyFormat) {
        const [int, dec] = this.getPrice(price)
        return (
            <p className="c-cart-item__price c-price">
                <span className="c-price__tag c-price--inverse">{currencyFormat}</span>
                <strong className="c-price__tag c-price--inverse"> {int}</strong>
                <span className="c-price__tag c-price--inverse">,{dec}</span>
            </p>
        )
    }

    render() {
        const {
            title,
            image,
            description,
            availableSizes,
            style,
            quantity,
            price,
            currencyFormat,
            onRemove
        } = this.props

        return (
            <a className={`c-cart-item ${this.state.hover ? 'c-cart-item--is-hovered' : ''}`}>
                <div className="c-media-object">
                    <div className="c-media-object__image c-media-object__image--left"><img className="c-cart-item__image" src={images[image]} alt={title} /></div>
                    <section className="c-media-object__content">
                        <header className="c-cart-item__title">{`${title} ${description}`}</header>
                        <p className="c-cart-item__description">{`${availableSizes[0]} | ${style}`}</p>
                        <p className="c-cart-item__description">Quantidade: {quantity}</p>
                        {this.renderPrice(price, currencyFormat)}
                        <button onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} onClick={onRemove} className="c-cart-item__remove">Remove from cart</button>
                    </section>
                </div>
            </a>
        );
    }
}

CartItem.propTypes = propTypes;

export default CartItem;
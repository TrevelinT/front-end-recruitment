import React from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import CartItem from './CartItem';
import Summary from './Summary';
import '../../Shared/styles/button.css';
import '../../Shared/styles/cart.css';
import { getCartQuantity, getCartTotal, getProductsInCart } from '../selectors';
import { getProductById } from '../../Products/selectors';
import { removeItem } from '../actions';

function Cart({
    cartQuantity,
    total,
    installments,
    products,
    removeProductFromCart
}) {
    function renderItems(products) {
        return products.map(product => (
            <CartItem key={product.id} {...product} onRemove={() => removeProductFromCart(product.id)} />
        ));
    }

    return (
        <div>
            <Header productQuantity={cartQuantity} />
            
            {products.length
                ? (
                    <div>
                        <div className="c-cart-list">
                            {renderItems(products)}
                        </div>
                        <Summary
                            total={total}
                            installments={installments}
                            currency="R$"
                        />
                        <div className="c-cart-buy">
                            <button className="c-button c-button--expanded c-button--primary">Comprar</button>
                        </div>
                    </div>
                )
                : (
                    <p className="c-cart-empty">O carrinho está vazio</p>
                )
            }
        </div>
    )
}

export { Cart };

function mapStateToProps(state) {
    const products = Object.values(getProductsInCart(state)).map(product => ({
        ...getProductById(product.product, state),
        quantity: product.quantity
    }))

    return {
        cartQuantity: getCartQuantity(state),
        total: products.reduce((sum, product) => sum + product.price, 0),
        installments: products.map(product => product.installments).sort().reverse()[0],
        products,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        removeProductFromCart: (id) => {
            dispatch(removeItem(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
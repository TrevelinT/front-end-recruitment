import React from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/cart.css';

const propTypes = {
    total: PropTypes.number.isRequired,
    installments: PropTypes.number.isRequired,
};

function getInstallments(total, installments) {
    return total / installments;
}

function getPrice(price) {
    return price.toFixed(2).split('.');
}

function Summary({
    total,
    installments,
    currency
}) {
    const [intTotal, decTotal] = getPrice(total)
    const [int, dec] = getPrice(getInstallments(total, installments))

    return (
        <section className="c-cart-summary">
            <header className="c-cart-summary__title">Subtotal</header>
            <div className="c-cart-summary__total">
                <p className="c-cart-summary__price c-price">
                    <span className="c-price__tag c-price--inverse">{currency}</span>
                    <strong className="c-price__tag c-price--inverse"> {intTotal}</strong>
                    <span className="c-price__tag c-price--inverse">,{decTotal}</span>
                </p>
                {installments > 0 && <p className="c-cart-summary__installments">{`Ou em até ${installments} x de ${currency} ${int},${dec}`}</p>}
            </div>
        </section>
    )
}

Summary.propTypes = propTypes;

export default Summary;

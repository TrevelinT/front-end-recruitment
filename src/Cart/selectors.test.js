import configureStore from 'redux-mock-store'
import { NAME } from './constants'
import { getProductsInCart, getCartQuantity, getCartTotal, getProductList } from './selectors';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('cart selectors', () => {
    it('should get cart items', () => {
        const store = mockStore({ [NAME]: {} })

        expect(getProductsInCart(store.getState())).toEqual({});
    });

    it('should get quantity of items in cart', () => {
        const store = mockStore({ [NAME]: {} })

        expect(getCartQuantity(store.getState())).toEqual(0);
    });

    it('should get the sum of all items in cart', () => {
        const store = mockStore({ [NAME]: {
            "1": { price: 100 },
            "2": { price: 150 }
        } })

        expect(getCartTotal(store.getState())).toEqual(250);
    });

    it('should get the product list in cart', () => {
        const store = mockStore({ [NAME]: {
            "1": { price: 100 },
            "2": { price: 150 }
        } })

        expect(getProductList(store.getState())).toEqual([
            { price: 100 },
            { price: 150 }
        ]);
    });

    it('should get the product list in cart and return an empty array', () => {
        const store = mockStore({ [NAME]: {} })

        expect(getProductList(store.getState())).toEqual([]);
    });
});
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CartItem from '../components/CartItem';
import Header from '../components/Header';
import Summary from '../components/Summary';
import '../../Shared/styles/button.css';

storiesOf('Cart', module)
    .add('Header', () => (
        <div style={{'background-color': '#000'}}>
            <Header productQuantity={100} />
        </div>
    ))
    .add('Item', () => (
        <CartItem
            title="Camisa Nike Corinthians I"
            image="item01.jpg"
            description="Branco com listras pretas"
            availableSizes={["GG"]}
            style="Branco com listras pretas"
            currencyFormat="R$"
            quantity={2}
            price={229.9}
            onRemove={action('Removeu produto')}
        />
    ))
    .add('Summary', () => (
        <div style={{ 'background-color': '#000' }}>
            <Summary
                total={100}
                installments={9}
                currency="R$"
            />
        </div>
    ))
    .add('Button', () => (
        <button onClick={action('Clicou botão')} className="c-button c-button--expanded c-button--primary">Comprar</button>
    ))
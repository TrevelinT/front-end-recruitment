import * as types from './actionTypes'

const initialState = {}

export default function cartReducer(previousState, { type, payload }) {
    previousState = previousState || initialState
    switch (type) {
        case types.ADD_ITEM:
            return {
                ...previousState,
                [payload.item]: {
                    product: payload.item,
                    quantity: payload.quantity
                }
            }
        case types.REMOVE_ITEM:
            return Object
                .keys(previousState)
                .filter(productId => productId != payload.item)
                .reduce((products, productId) => ({
                    ...products,
                    [productId]: previousState[productId]
                }), {})
        case types.CHANGE_ITEM_QUANTITY:
            return {
                ...previousState,
                [payload.item]: {
                    ...previousState[payload.item],
                    quantity: payload.quantity
                }
            }
        default:
            return previousState
    }
}
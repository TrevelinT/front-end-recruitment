import { NAME } from './constants'

export const ADD_ITEM = `${NAME}/ADD_ITEM`;
export const REMOVE_ITEM = `${NAME}/REMOVE_ITEM`;
export const CHANGE_ITEM_QUANTITY = `${NAME}/CHANGE_ITEM_QUANTITY`;
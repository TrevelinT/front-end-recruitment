import * as types from './actionTypes'

export function addItem(item, quantity) {
    return {
        type: types.ADD_ITEM,
        payload: {
            item,
            quantity
        }
    };
}

export function removeItem(item) {
    return {
        type: types.REMOVE_ITEM,
        payload: {
            item
        }
    };
}

export function changeItemQuantity(item, quantity) {
    return {
        type: types.CHANGE_ITEM_QUANTITY,
        payload: {
            item,
            quantity
        }
    };
}
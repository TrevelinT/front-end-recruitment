import { NAME } from './constants';

export function getProductsInCart(state) {
    return state[NAME]
}

export function getCartQuantity(state) {
    return Object.keys(getProductsInCart(state)).length;
}

export function getProductList(state) {
    return Object.values(getProductsInCart(state));
}

export function getCartTotal(state) {
    return getProductList(state)
        .reduce(
            (products, product) => products + product.price,
        0);
}

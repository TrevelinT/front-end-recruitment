import * as types from './actionTypes';
import reducer from './reducer';

describe('cart reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({})
    })

    it('should handle ADD_ITEM', () => {
        expect(
            reducer({}, {
                type: types.ADD_ITEM,
                payload: {
                    item: '29738127839283',
                    quantity: 1,
                }
            })
        ).toEqual({
            '29738127839283': {
                product: '29738127839283',
                quantity: 1
            }
        });
    });

    it('should handle REMOVE_ITEM', () => {
        expect(
            reducer({
                '29738127839283': {
                    product: '29738127839283',
                    quantity: 1
                },
                '82738728372873': {
                    product: '82738728372873',
                    quantity: 2
                },
            },
            {
                type: types.REMOVE_ITEM,
                payload: {
                    item: '29738127839283'
                }
            })
        ).toEqual({
            '82738728372873': {
                product: '82738728372873',
                quantity: 2
            }
        })
    })

    it('should handle CHANGE_ITEM_QUANTITY', () => {
        expect(
            reducer({
                '29738127839283': {
                    product: '29738127839283',
                    quantity: 1
                }
            },
            {
                type: types.CHANGE_ITEM_QUANTITY,
                payload: {
                    item: '29738127839283',
                    quantity: 3,
                }
            })
        ).toEqual({
            '29738127839283': {
                product: '29738127839283',
                quantity: 3
            }
        });
    });
});
import { combineReducers } from 'redux';
import productList from '../ProductList/reducer';
import products from '../Products/reducer';
import cart from '../Cart/reducer';

export default combineReducers({
    productList,
    products,
    cart
});
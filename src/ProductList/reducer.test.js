import * as actions from '../Products/actions';
import reducer from './reducer';

const initialState = [];

describe('productList reducer', () => {
    it('Should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('Should handle FETCH_SUCCESS', () => {
        const response = {
            products: [
                { id: 1 },
                { id: 2 }
            ]
        };
        const products = ["1", "2"];

        expect(reducer(initialState, actions.fetchSuccess(response))).toEqual(products);
    });
});
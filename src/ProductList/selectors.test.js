import configureStore from 'redux-mock-store'
import { NAME } from './constants'
import { getProductList } from './selectors';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('productList selectors', () => {
    it('should get all products', () => {
        const products = ['1', '2']
        const store = mockStore({
            [NAME]: products
        });

        expect(getProductList(store.getState())).toEqual(products);
    });
});
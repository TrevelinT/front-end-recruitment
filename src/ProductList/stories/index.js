import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ProductItem from '../components/ProductItem';
import { ProductList } from '../components/ProductList';

const ITEMS = [
    {
        title: 'Camisa Nike Corinthians I',
        image: 'item01.jpg',
        description: 'Branco com listras pretas',
        availableSizes: ['GG'],
        style: 'Branco com listras pretas',
        currencyFormat: 'R$',
        installments: 9,
        price: 229.9,
        onAdd: action('adicionou produto')
    },
    {
        title: 'Camisa Nike Corinthians I',
        image: 'item02.jpg',
        description: 'Branco com listras pretas',
        availableSizes: ['GG'],
        style: 'Branco com listras pretas',
        currencyFormat: 'R$',
        installments: 9,
        price: 229.9,
        onAdd: action('adicionou produto')
    },
    {
        title: 'Camisa Nike Corinthians I',
        image: 'item03.jpg',
        description: 'Branco com listras pretas',
        availableSizes: ['GG'],
        style: 'Branco com listras pretas',
        currencyFormat: 'R$',
        installments: 9,
        price: 229.9,
        onAdd: action('adicionou produto')
    }
];

storiesOf('ProductList', module)
    .add('Item', () => (
        <ProductItem
            title="Camisa Nike Corinthians I"
            image="item01.jpg"
            description="Branco com listras pretas"
            availableSizes={["GG"]}
            style="Branco com listras pretas"
            currencyFormat="R$"
            installments={9}
            price={229.9}
            onAdd={action('adicionou produto')}
        />
    ))
    .add('List', () => (
        <ProductList items={ITEMS} fetchProductListIfNeeded={action('Buscou produtos no servidor')}/>
    ))
import * as types from '../Products/actionTypes';

const initialState = [];

export default function productListReducer(previousState, { type, payload }) {
    previousState = previousState || initialState;
    
    switch (type) {
        case types.FETCH_SUCCESS:
            return Object.keys(payload);
    
        default:
            return previousState;
    }
}
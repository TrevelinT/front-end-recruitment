import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';
import '../../Shared/styles/grid.css';
import '../../Shared/styles/product.css';
import { fetchProductListIfNeeded } from '../../Products/actions';
import { getProductList } from '../selectors';
import { getProductById } from '../../Products/selectors';
import { addItem } from '../../Cart/actions';
import { getProductsInCart } from '../../Cart/selectors';

const propTypes = {
    items: PropTypes.array.isRequired,
    fetchProductListIfNeeded: PropTypes.func.isRequired
};

class ProductList extends Component {

    componentWillMount() {
        this.props.fetchProductListIfNeeded()
    }

    renderProducts(items) {
        return items.map(item => (
            <li
                key={item.id}
                className="l-grid__item"
            >
                <ProductItem 
                    onAdd={() => this.props.addProductToCart(item.id, 1)}
                    {...item}
                />
            </li>
        ))
    }

    renderNoProductsInCart() {
        return <p>Não há produtos disponíveis</p>
    }

    render() {
        const { items } = this.props
        
        return (
            <div className="l-container c-product-list">
                <ul className="l-grid">
                    {this.props.items.length
                        ? this.renderProducts(this.props.items)
                        : this.renderNoProductsInCart()
                    }
                </ul>
            </div>
        )
    }
}

ProductList.propTypes = propTypes;

// Add container here
export { ProductList };

function mapStateToProps(state) {
    const productsInCart = getProductsInCart(state)
    return {
        items: getProductList(state).map(id => ({
            ...getProductById(id, state),
            isInCart: !!productsInCart[id]
        }))
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProductListIfNeeded: () => {
            dispatch(fetchProductListIfNeeded())
        },
        addProductToCart: (id, quantity) => {
            dispatch(addItem(id, quantity))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
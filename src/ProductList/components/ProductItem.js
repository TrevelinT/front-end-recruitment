import React from 'react';
import PropTypes from 'prop-types';
import '../../Shared/styles/product.css'
import '../../Shared/styles/button.css'
import item01 from '../../Shared/images/item01.jpg'
import item02 from '../../Shared/images/item02.jpg'
import item03 from '../../Shared/images/item03.jpg'
import item04 from '../../Shared/images/item04.jpg'
import item05 from '../../Shared/images/item05.jpg'
import item06 from '../../Shared/images/item06.jpg'
import item07 from '../../Shared/images/item07.jpg'
import item08 from '../../Shared/images/item08.jpg'
import item09 from '../../Shared/images/item09.jpg'

const images = {
    'item01.jpg': item01,
    'item02.jpg': item02,
    'item03.jpg': item03,
    'item04.jpg': item04,
    'item05.jpg': item05,
    'item06.jpg': item06,
    'item07.jpg': item07,
    'item08.jpg': item08,
    'item09.jpg': item09,
}

const propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    installments: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    currencyFormat: PropTypes.string.isRequired,
    onAdd: PropTypes.func.isRequired,
    isInCart: PropTypes.bool,
};

function getInstallments(total, installments) {
    return total / installments;
}

function getPrice(price) {
    return price.toFixed(2).split('.');
}

function ProductItem({
    title,
    image,
    description,
    installments,
    price,
    currencyFormat,
    onAdd,
    isInCart
}) {
    const [intTotal, decTotal] = getPrice(price)
    const [int, dec] = getPrice(getInstallments(price, installments))

    return (
        <div className="c-product">
            <img src={images[image]} alt="" className="c-product__image"/>
            <p className="c-product__title">{`${title} ${description}`}</p>
            <p className="c-product__price c-price">
                <span className="c-price__tag">{currencyFormat}</span>
                <strong className="c-price__tag c-price__tag--emphasis"> {intTotal}</strong>
                <span className="c-price__tag">,{decTotal}</span>
            </p>
            <p className="c-product__installments">{installments > 0 && `ou ${installments} x de ${currencyFormat} ${int},${dec}`}</p>
            <button disabled={isInCart} className="c-button c-button--primary" onClick={onAdd}>{isInCart ? 'Na Sacola' : 'Comprar'}</button>
        </div>
    );
}

ProductItem.propTypes = propTypes;

export default ProductItem;
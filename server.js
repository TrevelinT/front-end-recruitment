const express = require('express');
const path = require('path');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const port = process.env.PORT || 8080;
const app = express();
const config = require('./webpack.config');
const compiler = webpack(config);
const data = require('./public/data/products.json');

app.use(express.static(__dirname + '/Shared'));

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath
}));

app.use(webpackHotMiddleware(compiler));

app.get('/api/products', function(req, res) {
    res.status(200).json(data)
})

app.get('/', function(req, res, next) {
    const filename = path.join(compiler.outputPath, 'index.html');
    compiler.outputFileSystem.readFile(filename, function(err, result) {
        if (err) {
            return next(err);
        }
        res.set('content-type', 'text/html');
        res.send(result);
        res.end();
    });
});

app.listen(port);
console.log('Server started');
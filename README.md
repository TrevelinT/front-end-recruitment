# Netshoes Cart Test

## How to run the project

Clone this repository and install its contents
``` js
npm install
```

To run the project, type
``` js
npm run start
```
Whis will set up a running server in http://localhost:8080

To run the project's unit test, type
``` js
npm run test
```

To run the project's unit test and see test coverage, type
``` js
npm run test:all
```

To see project's components in storybook, type
``` js
npm run storybook
```

Whis will set up a running server in http://localhost:9001

Below is the original readme:

## Expected result

* See a list of products;
* Be able to add or remove items to the cart and get instant visual feedback;
* See the products added to the cart.

## Data

The data we provide is a static JSON file under `/public/data`.

## Notes

* You can use whatever stack or tooling you want to help you;
* Feel free to ask us questions during the process (but trust your guts, please!);
* You should create a static server in order to access the JSON data provided.

## Bonus

* Persist data on page reload;
* Test your code;
* Instructions on how to build/run the project.

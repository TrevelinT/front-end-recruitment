import { configure } from '@storybook/react';
import '../src/index.css'

function loadStories() {
    require('../src/Cart/stories/index.js');
    require('../src/ProductList/stories/index.js');
    require('../src/App/stories/index.js');
}

configure(loadStories, module);